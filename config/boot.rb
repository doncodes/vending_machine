ENV['BUNDLE_GEMFILE'] ||= File.expand_path('../../Gemfile', __FILE__ )

require 'bundler/setup'
Bundler.require
require 'logger'
require 'forwardable'
require 'yaml'

require_relative 'app'

Dir['./lib/concerns/*.rb'].each { |f| require f }
Dir['./lib/payment_system/*.rb'].each { |f| require f }
Dir['./lib/vending_machine/*.rb'].each { |f| require f }
Dir['./lib/*.rb'].each { |f| require f }

Dir['./config/initializers/*.rb'].each { |f| require f }
