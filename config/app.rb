module App
  class << self
    extend Forwardable

    def init
      App.info "App started in #{App.env}"

      VendingMachine.start
    end

    def close(code)
      VendingMachine.save_state unless test?

      App.info "See you soon! App closed."

      exit(code)
    end

    def parse_configs(path)
      YAML.load(File.read("#{root}/#{path}"))[env]
    end

    def save_to_file(path, data)
      envs_data = YAML.load(File.read("#{root}/#{path}"))
      envs_data[env].merge!(data)

      File.open("#{root}/#{path}", 'w') { |f| f.write(envs_data.to_yaml) }
    end

    def environment
      @environment ||= ENV['VENDING_MACHINE_ENV'] || 'development'
    end
    alias env environment

    def test?
      env == 'test'
    end

    private

    def root
      @root ||= File.dirname(File.expand_path('..', __FILE__))
    end

    def_delegators :AppLogger, :info, :error, :fatal, :warn
  end
end