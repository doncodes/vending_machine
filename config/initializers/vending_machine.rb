path = 'config/vending_machine.yml'
configs = App.parse_configs(path)

VendingMachine.configure(path, configs)