module VendingMachine
  class Storage
    extend Forwardable

    def initialize(vending_machine, slots)
      @vending_machine = vending_machine
      @slots = slots.map.with_index(1) { |product, idx| [idx, Slot.new(idx, product)] }.to_h
    end

    def save
      App.save_to_file(source_path, { storage: @slots.values.map(&:as_json) })
    end

    def render
      @slots.values.map do |slot|
        slot.num % 3 == 0 ? "     #{slot.render}     \n" : "     #{slot.render}     "
      end.join
    end

    def ===(slot_num)
      @slots.key?(slot_num) && @slots[slot_num].available?
    end

    def_delegator :@slots, :[], :pick
    def_delegator :@vending_machine, :source_path
  end
end