module VendingMachine
  class << self
    include Communicable

    attr_reader :source_path

    def configure(source_path, storage:, bank:)
      @source_path = source_path

      @storage = VendingMachine::Storage.new(self, storage)
      @payment_system = PaymentSystem::Manager.new(self, bank)
    end

    def start
      App.info "\n#{@storage.render}\n\tWhat you would like to order? Input number from available slots. Press x/X to exit."

      slot = choose_slot

      @payment_system.grab_money(slot)

      slot.subtract_product!

      App.info 'Something more? Press any key to continue or x/X to leave'

      ask_user.match?(/^x|X/) ? App.close(0) : start
    end

    def save_state
      @storage.save

      @payment_system.save
    end

    private

    def choose_slot
      case slot_num = ask_user.to_i
      when @storage then @storage.pick(slot_num)
      when 0 then App.close(1)
      else
        App.info 'Wrong slot number. Try another one or press x/X to exit.'

        choose_slot
      end
    end
  end
end