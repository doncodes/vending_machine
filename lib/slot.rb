class Slot
  extend Forwardable

  attr_reader :num

  def initialize(num, product)
    @num = num
    @product = Product.new(**product)
  end

  def available?
    @product.amount > 0
  end

  def subtract_product!
    @product.amount -= 1

    self
  end

  def render
    if available?
      "| #{(@product.to_s + " (press: #{@num}#{", last" if @product.last?})").ljust(40)} |".green
    else
      "| #{@product.to_s.ljust(40)} |".red
    end
  end

  def_delegators :@product, :price, :to_s, :as_json
end