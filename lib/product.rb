class Product
  attr_reader :price
  attr_accessor :amount

  def initialize(name:, amount:, price:)
    @name = name
    @amount = amount
    @price = price
  end

  def last?
    @amount == 1
  end

  def as_json(options = nil)
    { name: @name, amount: @amount, price: @price }
  end

  def to_s
    " #{@name}: $#{@price} "
  end
end