module PaymentSystem
  class Manager
    include Communicable
    extend Forwardable

    def initialize(vending_machine, bank)
      @vending_machine =  vending_machine

      @bank = PaymentSystem::Bank.new(bank)
    end

    def grab_money(slot)
      App.info "You chose #{slot}. Put money with value:#{@bank.valid_coins} or press x/X to exit"

      @deposit = PaymentSystem::Money.new

      while
        @deposit << get_money

        break if @deposit >= slot.price

        App.info(
          "Please input at least $#{(slot.price - @deposit.to_f).round(2)} more or press x/X to exit." +
            "(Vending machine will not return deposited money.)".underline
        )
      end

      @bank.deposit_funds(@deposit, (@deposit - slot.price).round(2))
    end

    def save
      App.save_to_file(source_path, { bank: @bank.as_json })
    end

    private

    def get_money
      case money = ask_user.to_f.round(2)
      when @bank then money
      when 0
        @bank.deposit_funds(@deposit)

        App.close(1)
      else
        App.info "We accept only coins with value(#{@bank.valid_coins}). Try again or press x/X to exit"

        get_money
      end
    end

    def_delegator :@vending_machine, :source_path
  end
end