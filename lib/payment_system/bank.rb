module PaymentSystem
  class Bank
    attr_reader :valid_coins

    def initialize(bank)
      @bank = bank

      @valid_coins = bank.keys
    end

    def deposit_funds(money, change=0)
      money.each { |coin| @bank[coin] += 1 }

      return if change == 0

      coins = give_change(change)

      App.info "Take your change, #{coins}." unless coins == 0
      App.info "We are truly sorry that vending machine didn't have "\
        "enough of money for returning rest of change. Please call." if coins != change

      self
    end

    def ===(money)
      @valid_coins.include?(money)
    end

    def as_json(options=nil)
      @bank
    end

    private

    def give_change(change)
      coins = PaymentSystem::Money.new

      available_coins.each do |coin|
        break if change == 0
        next if change/coin < 1

        div = (change/coin) > @bank[coin] ? @bank[coin] : (change/coin).to_i

        @bank[coin] -= div

        coins.concat(Array.new(div, coin))

        change = (change - coin * div).round(2)
      end

      coins
    end

    def available_coins
      @bank.select { |_, amount| amount > 0 }.keys
    end
  end
end