module PaymentSystem
  class Money
    include Comparable
    extend Forwardable

    def initialize
      @coins = []
    end

    def -(money)
      @coins.sum - money
    end

    def <=>(money)
      @coins.sum <=> money
    end

    def to_s
      h = Hash.new(0)

      @coins.each {|v| h[v] +=1 }

      h.map { |coin, amount| "$#{coin}(#{amount} c.)" }.join(', ')
    end

    def to_f
      @coins.sum
    end

    private

    def_delegators :@coins, :concat, :<<, :each
  end
end