ENV['VENDING_MACHINE_ENV'] = 'test'

require File.expand_path('../../config/boot', __FILE__)

RSpec.configure do |config|
  config.before(:each) { AppLogger.configure StringIO.new }

  config.formatter = :documentation
end

def logged
  AppLogger.output.string
end