RSpec.describe VendingMachine do
  subject { App.init }

  before { $stdin = StringIO.new(input) }

  context 'buy without retry and without change' do
    let(:input) { "1\n5\nX\n" }

    it 'user buys without surprises' do
      subject
    rescue SystemExit => e
      expect(e.status).to eq(0)

      expect(logged).to include(
                       'What you would like to order?',
                       'You chose  snickers: $5',
                       'Something more? Press any key to continue or x/X to leave'
                     )

      expect(logged).to_not include(
                             'Wrong slot number',
                             'Please input at least',
                             'We accept only coins',
                             'Take your change',
                             'We are truly sorry that vending machine',
                            )
    end
  end

  context 'buy after slot and money retry, with change' do
    let(:input) { "3\n8\n0.5\n12\n5\nX\n" }

    it 'buys sandwich_tuna after picking invalid slot. Sees log about valid_coins and valid slots.' do
      subject
    rescue SystemExit => e
      expect(e.status).to eq(0)

      expect(logged).to include(
                          'What you would like to order?',
                          'You chose  sandwich_tuna: $4.13',
                          'Please input at least $3.63 more',
                          'We accept only coins with value([5.0, 2.0, 1.0, 0.5, 0.25, 0.1, 0.05, 0.02, 0.01])',
                          'Take your change, $1.0(1 c.), $0.25(1 c.), $0.1(1 c.), $0.02(1 c.)',
                          'Something more? Press any key to continue or x/X to leave'
                        )
    end
  end

  context 'buy last one and making slot unavailable' do
    let(:input) { "2\n5\nyes\n2\nX\n" }

    it 'buys last mars for $2.55 and tries to buy one more. After failed attempt user exits' do
      subject
    rescue SystemExit => e
      expect(e.status).to eq(1)

      expect(logged).to include(
                          'You chose  mars: $2.55',
                          'Take your change, $2.0(1 c.), $0.25(1 c.), $0.1(2 c.)',
                          'Something more? Press any key to continue or x/X to leave',
                          'Wrong slot number. Try another one or press x/X to exit'
                        )
    end
  end

  context 'not enough change in the vending machine' do
    let(:input) { "9\n5\n0.5\nX\n" }

    it 'buys sandwich with lamb but vending machine do not have 0.01 for change and notifies about it' do
      subject
    rescue SystemExit => e
      expect(e.status).to eq(0)

      expect(logged).to include(
                          'You chose  sandwich_lamb: $5.49',
                          'Please input at least $0.49 more',
                          "We are truly sorry that vending machine didn't have enough of money for returning rest of change"
                        )

      expect(logged).to_not include(
                              'Take your change'
                            )
    end
  end
end